/***************************************************************************
 *   Copyright (C) %{CURRENT_YEAR} by %{AUTHOR} <%{EMAIL}>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#include "tubestest.h"
#include <KTp/debug.h>
#include <KTp/Widgets/contact-grid-dialog.h>
#include <KTp/contact-factory.h>
#include <telepathy-qt4/TelepathyQt/AccountFactory>
#include <telepathy-qt4/TelepathyQt/AccountManager>
#include <telepathy-qt4/TelepathyQt/StreamTubeClient>
#include <telepathy-qt4/TelepathyQt/StreamTubeServer>
#include <telepathy-qt4/TelepathyQt/PendingChannelRequest>
#include <QTcpServer>
#include <QTcpSocket>

Tubestest::Tubestest()
    : KMainWindow()
    , m_socket(0)
{
    QWidget* w = new QWidget(this);
    setCentralWidget(w);
    m_ui.setupUi(w);
    m_ui.pingButton->setEnabled(false);

    Tp::registerTypes();
    KTp::Debug::installCallback(true);

    Tp::AccountFactoryPtr  accountFactory = Tp::AccountFactory::create(QDBusConnection::sessionBus(),
                                                                       Tp::Features() << Tp::Account::FeatureCore
                                                                       << Tp::Account::FeatureAvatar
                                                                       << Tp::Account::FeatureProtocolInfo
                                                                       << Tp::Account::FeatureProfile);

    Tp::ConnectionFactoryPtr connectionFactory = Tp::ConnectionFactory::create(QDBusConnection::sessionBus(),
                                                                               Tp::Features() << Tp::Connection::FeatureCore
                                                                               << Tp::Connection::FeatureRosterGroups
                                                                               << Tp::Connection::FeatureRoster
                                                                               << Tp::Connection::FeatureSelfContact);

    Tp::ContactFactoryPtr contactFactory = KTp::ContactFactory::create(Tp::Features()  << Tp::Contact::FeatureAlias
                                                                      << Tp::Contact::FeatureAvatarData
                                                                      << Tp::Contact::FeatureSimplePresence
                                                                      << Tp::Contact::FeatureCapabilities);

    Tp::ChannelFactoryPtr channelFactory = Tp::ChannelFactory::create(QDBusConnection::sessionBus());

    m_accountManager = Tp::AccountManager::create(QDBusConnection::sessionBus(),
                                                  accountFactory,
                                                  connectionFactory,
                                                  channelFactory,
                                                  contactFactory);

    m_streamClient = Tp::StreamTubeClient::create(m_accountManager, QStringList() << "tubetest", QStringList(), QString(), true, true);
    m_streamClient->setToAcceptAsTcp();
    connect(m_streamClient.data(), SIGNAL(connectionClosed(Tp::AccountPtr,Tp::IncomingStreamTubeChannelPtr,uint,QString,QString)),
            this, SLOT(clientTubeClosed(Tp::AccountPtr,Tp::IncomingStreamTubeChannelPtr,uint,QString,QString)));
    m_server = new QTcpServer(this);
    connect(m_server, SIGNAL(newConnection()), this, SLOT(onTcpServerNewConnection()));
    m_server->listen();

    m_tubeServer = Tp::StreamTubeServer::create(m_accountManager, QStringList() << "tubetest");
    connect(m_tubeServer.data(), SIGNAL(tubeClosed(Tp::AccountPtr,Tp::OutgoingStreamTubeChannelPtr,QString,QString)),
            this, SLOT(tubeClosed(Tp::AccountPtr,Tp::OutgoingStreamTubeChannelPtr,QString,QString)));
    m_tubeServer->exportTcpSocket(m_server);

    connect(m_ui.createTubeButton, SIGNAL(clicked(bool)),
            this, SLOT(onCreateTubeButtonClicked()));

    connect(m_streamClient.data(),
            SIGNAL(tubeAcceptedAsTcp(QHostAddress,quint16,QHostAddress,quint16,Tp::AccountPtr,Tp::IncomingStreamTubeChannelPtr)),
            SLOT(onClientTubeCreated(QHostAddress,quint16,QHostAddress,quint16,Tp::AccountPtr,Tp::IncomingStreamTubeChannelPtr)));

    connect(m_ui.pingButton, SIGNAL(clicked(bool)), this, SLOT(sendPing(bool)));
    connect(QApplication::instance(), SIGNAL(aboutToQuit()), this, SLOT(quit()));
}

void Tubestest::clientTubeClosed(Tp::AccountPtr , Tp::IncomingStreamTubeChannelPtr , uint , QString err, QString msg)
{
    qDebug() << "client: tube closed!";
    qDebug() << err << msg;
}

void Tubestest::quit()
{
    if ( m_socket ) {
        m_socket->close();
    }
    if ( m_incoming ) {
        m_incoming->requestClose();
    }
    if ( m_outgoing ) {
        m_outgoing->requestClose();
    }
}

void Tubestest::tubeClosed(Tp::AccountPtr , Tp::OutgoingStreamTubeChannelPtr , QString err, QString msg)
{
    qDebug() << "server: tube closed!";
    qDebug() << err << msg;
}

void Tubestest::sendPing(bool )
{
    qDebug() << "sending ping";
    m_socket->write("PING!\n");
    m_socket->flush();
}

void Tubestest::finalize()
{
    connect(m_socket, SIGNAL(readyRead()), this, SLOT(dataReceived()));
}

void Tubestest::dataReceived()
{
    qDebug() << "data received";
    m_ui.label->setText(m_ui.label->text() + qobject_cast<QTcpSocket*>(QObject::sender())->readAll());
}

void Tubestest::onCreateTubeButtonClicked()
{
    qDebug() << "create tube button clicked";
    KTp::ContactGridDialog dialog(m_ui.label);
    if (dialog.exec()) {
        qDebug() << "creating channel request" << dialog.contact();
        Tp::PendingChannelRequest* channelRequest = 0;
        channelRequest = dialog.account()->createStreamTube(dialog.contact(),
                                                            QLatin1String("tubetest"),
                                                            QDateTime::currentDateTime(),
                                                            "org.freedesktop.Telepathy.Client.KTp.tubetest");

        connect(channelRequest, SIGNAL(finished(Tp::PendingOperation*)),
                this, SLOT(onCreateTubeFinished(Tp::PendingOperation*)));
    }
}

void Tubestest::onClientTubeCreated(QHostAddress listenAddress, quint16 listenPort, QHostAddress ,
                                    quint16 , Tp::AccountPtr , Tp::IncomingStreamTubeChannelPtr incoming)
{
    qDebug() << "client tube created!";
    m_socket = new QTcpSocket;
    connect(m_socket, SIGNAL(connected()), this, SLOT(clientOk()));
    connect(m_socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(clientError(QAbstractSocket::SocketError)));
    m_socket->connectToHost(listenAddress, listenPort);
    m_incoming = incoming;
}

void Tubestest::clientOk()
{
    qDebug() << "client connection ok!";
    m_ui.pingButton->setEnabled(true);
    finalize();
}

void Tubestest::clientError(QAbstractSocket::SocketError err)
{
    qDebug() << "client connection error: QAbstractSocket::SocketError" << err;
}

void Tubestest::onCreateTubeFinished(Tp::PendingOperation* op)
{
    qDebug() << "create tube finished";
    m_outgoing = qobject_cast<Tp::PendingChannelRequest*>(op)->channelRequest()->channel();
}

void Tubestest::onTcpServerNewConnection()
{
    qDebug() << "new connection";
    m_socket = m_server->nextPendingConnection();
    m_ui.pingButton->setEnabled(true);
    finalize();
}

Tubestest::~Tubestest()
{
}

#include "tubestest.moc"
