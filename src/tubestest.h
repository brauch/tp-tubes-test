/***************************************************************************
 *   Copyright (C) %{CURRENT_YEAR} by %{AUTHOR} <%{EMAIL}>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#ifndef TUBESTEST_H
#define TUBESTEST_H

#include <KDE/KMainWindow>
#include <telepathy-qt4/TelepathyQt/Types>
#include <telepathy-qt4/TelepathyQt/StreamTubeChannel>
#include <telepathy-qt4/TelepathyQt/IncomingStreamTubeChannel>
#include <telepathy-qt4/TelepathyQt/OutgoingStreamTubeChannel>
#include <telepathy-qt4/TelepathyQt/StreamTubeClient>
#include <telepathy-qt4/TelepathyQt/StreamTubeServer>
#include <telepathy-qt4/TelepathyQt/AccountManager>
#include <QTcpServer>
#include <QTcpSocket>

#include "ui_tubestest.h"

namespace Tp {
class StreamTubeClient;
}

class Tubestest : public KMainWindow
{
    Q_OBJECT
    Tp::AccountManagerPtr m_accountManager;
public:
    Tubestest();
    virtual ~Tubestest();

private:
    Ui::mainWidget m_ui;
    Tp::StreamTubeClientPtr m_streamClient;
    QTcpServer* m_server;
    Tp::StreamTubeServerPtr m_tubeServer;
    QTcpSocket* m_socket;
    Tp::IncomingStreamTubeChannelPtr m_incoming;
    Tp::ChannelPtr m_outgoing;

public slots:
    void onTcpServerNewConnection();
    void onCreateTubeButtonClicked();
    void onCreateTubeFinished(Tp::PendingOperation*);
    void onClientTubeCreated(QHostAddress,quint16,QHostAddress,quint16,Tp::AccountPtr,Tp::IncomingStreamTubeChannelPtr);
    void clientOk();
    void clientError(QAbstractSocket::SocketError);
    void sendPing(bool);
    void finalize();
    void dataReceived();
    void tubeClosed(Tp::AccountPtr,Tp::OutgoingStreamTubeChannelPtr,QString,QString);
    void clientTubeClosed(Tp::AccountPtr,Tp::IncomingStreamTubeChannelPtr,uint,QString,QString);
    void quit();
};

#endif // _TUBESTEST_H_
